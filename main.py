from datetime import datetime
import logging

from config import OBSERVED_SUBJECTS, LOG_FILE
from get_mail import get_attachment_by_subject, auth_mail
from files import compare, replace_old_file
from sender import send
from tgbot import send_to_tg


def main():
    logging.basicConfig(level=logging.INFO, format="%(asctime)s [%(levelname)s] %(message)s", handlers=[
        logging.FileHandler(LOG_FILE, mode="a"),
        logging.StreamHandler()])

    start = datetime.now()
    send_to_tg(f"<b>--------- СКРИПТ ЗАПУЩЕН {start.strftime('%Y-%m-%d %H:%M:%S')} ---------</b>")
    mail = auth_mail()
    for subject in OBSERVED_SUBJECTS:
        try:
            io = get_attachment_by_subject(mail, subject)
            if io is None:
                continue  # Нет вложений
            column_names = io.readline()  # Не сравнивать первые строки
            changed_lines = compare(subject, io)
            if len(changed_lines) == 0:
                continue  # Нет измененных строк
            send(column_names, changed_lines, subject)
            replace_old_file(subject, io)
        except Exception as e:
            logging.exception("Exception here:")
            send_to_tg(e)
            continue
    mail.logout()
    send_to_tg(f"<b>RUNTIME : {datetime.now() - start}</b>")


if __name__ == '__main__':
    main()
