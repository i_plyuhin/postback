import os
import io
from glob import glob

from tgbot import send_to_tg


def replace_old_file(subject: str, io: io.BytesIO) -> None:
    old_file = _open_old_file(subject, "wb")
    io.seek(0)
    for line in io:
        old_file.write(line)
    old_file.close()


def compare(subject: str, io: io.BytesIO) -> list:
    changed_lines = []
    old_file = _open_old_file(subject, 'rb')
    old_file_set = set(old_file)
    count = 0
    for line in io:
        count += 1
        if line not in old_file_set:
            changed_lines.append(line)
    send_to_tg(f"В файле {count} строк, {len(changed_lines)} измененных")
    old_file.close()
    return changed_lines


def _open_old_file(subject: str, mode: str):
    try:
        old_file = f"{glob(f'./storage/{subject}/*.csv')[0]}"
        return open(old_file, mode)
    except IndexError:
        path = f"./storage/{subject}/{subject}.csv"
        os.mknod(path)
        return open(path, mode)
