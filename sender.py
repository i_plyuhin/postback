import csv
import requests
import logging
from datetime import datetime
from tgbot import send_to_tg

URL = 'https://amdgstat.ru/26bc008/postback?'
DEFAULT_INDICES = (2, 3, 6)
DEFAULT_DATEFORMAT = "%Y-%m-%d %H:%M:%S"
DATETIME_INDEX = 0
MAX_ORDER_AGE_DAYS = 60

indices_by_theme = {
    # "Тема": (is_ar1, is_deal_oplata, subid_utm_campaign)
    "Artox выгрузка по статусам ПК": (1, 6, 10),
    "Artox ГПБ Рефин выгрузка по статусам": (1, 6, 10),
    "Artox ГПБ АК и УК выгрузка по статусам": (3, 6, 10),
    "Artox ГПБ KK выгрузка по статусам": (5, 8, 14)
}

dateformat_by_theme = {
    "Artox выгрузка по статусам ПК": "%d.%m.%Y %H:%M",  # 01.10.2022 00:15;
    "Artox ГПБ Рефин выгрузка по статусам": "%d.%m.%Y %H:%M",  # 01.10.2022 00:35;
    "Artox ГПБ АК и УК выгрузка по статусам": DEFAULT_DATEFORMAT,  # 2022-01-02 00:34:27;
    "Artox ГПБ KK выгрузка по статусам": DEFAULT_DATEFORMAT,  # 2022-08-15 02:01:16
    "Artox ГПБ ДК ДКП выгрузка по статусам": DEFAULT_DATEFORMAT  # 2022-08-31 00:03:27
}


def send(column_names, changed_lines: list, subject: str):
    if "Artox ГПБ ДК ДКП выгрузка по статусам" in subject:
        _dk_dkp_send(column_names, changed_lines, subject)
    else:
        _artox_send(changed_lines, subject)


def _dk_dkp_send(column_names: bytes, changed_lines: list, subject: str):
    column_names = column_names.decode("cp1251").split(";")
    parsed_orders = list(csv.reader([x.decode("cp1251") for x in changed_lines], delimiter=";"))
    sent_count = 0
    subprogramcode_idx, utm_campaign_idx, is_transaction_idx, pos_activ_250_idx = _dk_dkp_get_idx(column_names)
    DK_AGENT_STATUS_INDEX = {
        "UnionPay 10": is_transaction_idx,
        "UnionPay Gold": is_transaction_idx,
        "МИР 9": is_transaction_idx,
        "JCB Gold": pos_activ_250_idx,
        "Visa Gold": pos_activ_250_idx,
        "МИР 76": pos_activ_250_idx,
        "МИРmc Classic": pos_activ_250_idx,
        # "Visa Infinite": is_transaction_idx,
        # "МИР Gold": is_transaction_idx,
        # "Visa Signature": pos_activ_250_idx,
    }
    not_found_subprogramcodes = set()

    for order in parsed_orders:
        subprogramcode = order[subprogramcode_idx]
        try:
            status_idx = DK_AGENT_STATUS_INDEX[subprogramcode]
        except KeyError as e:
            logging.exception("Exception here:")
            not_found_subprogramcodes.add(subprogramcode)
            continue

        status = _dk_dkp_define_status(order, status_idx)
        subid = order[utm_campaign_idx].split("|")[0]
        if subid not in ["-", "", "{subid}"] and status == "sale":  # лид со статусом sale
            dateformat = dateformat_by_theme.get(subject, DEFAULT_DATEFORMAT)
            order_date = datetime.strptime(order[DATETIME_INDEX], dateformat)
            if (datetime.today() - order_date).days > MAX_ORDER_AGE_DAYS:  # Проверяем что не старше 60 дней
                logging.info(f"skipping old order {subid}")
                continue
            sent_count += 1
            resp = requests.get(URL + f"subid={subid}&status={status}")
            logging.info(f'{resp.text} {subid} {order_date}')
    for code in not_found_subprogramcodes:
        send_to_tg(f"{code} нет в списке")

    send_to_tg(f"Отправленно {sent_count} постбэков")


def _dk_dkp_get_idx(column_names: list[str]) -> list[int]:
    values_to_find = ["SUBPROGRAMCODE", "utm_campaign", "is_transaction", "pos_activ_250"]
    indices = []
    for column_name in values_to_find:
        try:
            indices.append(column_names.index(column_name))
        except ValueError as e:
            logging.exception("Exception here:")
            send_to_tg(f"{column_name} не найден")
            raise e
    return indices


def _dk_dkp_define_status(order, idx):
    if order[idx] == "1":
        return "sale"
    return ""


def _artox_send(changed_lines: list, subject: str):
    parsed_orders = list(csv.reader([x.decode("latin-1") for x in changed_lines], delimiter=";"))
    sent_count = 0
    is_ar1_index, is_deal_oplata_index, subid_index = indices_by_theme.get(subject, DEFAULT_INDICES)
    for order in parsed_orders:
        status = _artox_define_status(order[is_ar1_index], order[is_deal_oplata_index])
        subid = order[subid_index].split("|")[0]
        if subid not in ["-", "", "{subid}"] and status == "sale":  # лид со статусом sale
            dateformat = dateformat_by_theme.get(subject, DEFAULT_DATEFORMAT)
            order_date = datetime.strptime(order[DATETIME_INDEX], dateformat)
            if (datetime.today() - order_date).days > MAX_ORDER_AGE_DAYS:  # Проверяем что не старше 60 дней
                logging.info(f"skipping old order {subid}")
                continue
            sent_count += 1
            resp = requests.get(URL + f"subid={subid}&status={status}")
            logging.info(f'{resp.text} {subid} {order_date}')

    send_to_tg(f"Отправленно {sent_count} постбэков")


def _artox_define_status(ar1: str, deal: str) -> str:
    if ar1 == "1" and deal == "0":
        return "lead"
    if ar1 == "1" and deal == "1":
        return "sale"
    if ar1 == "0" and deal == "0":
        return "rejected"
    return ""

# def _dk_agent_send(changed_lines: list, subject: str):
#     parsed_orders = list(csv.reader([x.decode("cp1251") for x in changed_lines], delimiter=","))
#     sent_count = 0
#     for order in parsed_orders:
#         status = _dk_define_status(order)
#         subid = order[DK_SUBID_INDEX].split("|")[0]
#         if subid not in ["-", "", "{subid}"] and status == "sale":
#             order_date = datetime.strptime(order[DATETIME_INDEX], DEFAULT_DATEFORMAT)
#             if (datetime.today() - order_date).days > MAX_ORDER_AGE_DAYS:  # Проверяем что не старше 60 дней
#                 logging.info(f"skipping old order {subid}")
#                 continue
#             sent_count += 1
#             resp = requests.get(URL + f"subid={subid}&status={status}")
#             logging.info(f'{resp.text} {subid} {order_date}')
#
#     send_to_tg(f"Отправленно {sent_count} постбэков")
#
#
# def _dk_define_status(order: list):
#     subprogram_code = order[DK_SUBPROGRAMCODE_INDEX]
#     status_index = DK_AGENT_STATUS_INDEX[subprogram_code]
#     status = order[status_index]
#     if status == "1":
#         return "sale"
#     else:
#         return ""
