import logging

import requests

from config import BOT_TOKEN, BOT_CHAT_ID, IS_TG_ENABLED


def send_to_tg(bot_message):
    if IS_TG_ENABLED:
        for chat_id in BOT_CHAT_ID:
            url = f'https://api.telegram.org/bot{BOT_TOKEN}/sendMessage?chat_id={chat_id}&parse_mode=HTML&text={bot_message}'
            try:
                requests.get(url, timeout=1)
            except Exception:
                pass

    logging.info(bot_message)
