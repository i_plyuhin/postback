import io
from imap_tools import MailBox, A
from datetime import timedelta

from tgbot import send_to_tg
from config import USERNAME, PASSWORD, HOST


def auth_mail() -> MailBox:
    return MailBox(HOST).login(USERNAME, PASSWORD)


def get_attachment_by_subject(mailbox: MailBox, subject: str) -> io.BytesIO:
    k = 0
    while k < 10:  # Яндекс иногда может ответить ошибкой: [UNAVAILABLE] SEARCH Backend error
        k += 1
        try:
            for msg in mailbox.fetch(A(subject=subject), charset='utf8', reverse=True):
                for att in msg.attachments:
                    send_to_tg(f'"{subject}" Новое вложение от {(msg.date + timedelta(hours=3)).strftime("%Y-%m-%d %H:%M:%S")}')
                    return io.BytesIO(att.payload)
        except:
            continue

    send_to_tg(f'"{subject}" Нет новых вложений')

